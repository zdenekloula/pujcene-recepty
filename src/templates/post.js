import React from "react"
import { graphql, Link } from "gatsby"
import SEO from "../components/seo"
import Layout from "../components/layout"
import Image from "gatsby-image"

function PostTemplate(props) {
  const post = props.data.wordpressPost
  return (
    <Layout>
      <SEO title={post.title} />
      <div>
        <h1
          className="article__title"
          dangerouslySetInnerHTML={{ __html: post.title }}
        />
        {post.featured_media && (
          <div className="article__image-wrapper">
            <Image
              fluid={post.featured_media.localFile.childImageSharp.fluid}
              imgStyle={{ objectFit: "cover" }}
              alt={post.title}
            />
          </div>
        )}
      </div>
      <div className="article__ingredients-wrapper">
        <div className="article__ingredients-box">
          <h2>Suroviny</h2>
          <ul className="article__ingredients-list">
            {post.acf.ingredients_list.map(ingredienceItem => {
              return (
                <li
                  className="article__ingredients-item"
                  key={ingredienceItem.ingredience}
                >
                  <div className="article__ingredients-item-inner">
                    {ingredienceItem.ingredience}
                  </div>
                </li>
              )
            })}
          </ul>
        </div>
      </div>
      <div className="article__content-wrapper">
        {post.content.length > 0 && (
          <div
            className="article__body-text"
            dangerouslySetInnerHTML={{ __html: post.content }}
          />
        )}
        <div className="article__steps">
          <div className="step-list">
            <ol className="step-list__list">
              {post.acf.steps.map((stepItem, index) => {
                return (
                  <li className="step-list__item" key={stepItem.step}>
                    <div className="step-list__number">
                      <span className="step-list__number-item">
                        {index + 1}
                      </span>
                      <span className="step-list__number-dot">.</span>
                    </div>
                    <div
                      className="step-list__content"
                      dangerouslySetInnerHTML={{ __html: stepItem.step }}
                    />
                  </li>
                )
              })}
            </ol>
          </div>
        </div>
        <div className="article__footer">
          <h3>
            Autor:
            <Link
              className="article__author-link"
              to={`/autor/${post.acf.author.slug}`}
            >
              <strong>{post.acf.author.name}</strong>
            </Link>
          </h3>
        </div>
      </div>
    </Layout>
  )
}

export default PostTemplate

export const pageQuery = graphql`
  query($id: String!) {
    wordpressPost(id: { eq: $id }) {
      title
      content
      acf {
        author {
          name
          description
          slug
        }
        ingredients_list {
          ingredience
        }
        steps {
          step
        }
      }
      featured_media {
        localFile {
          childImageSharp {
            fluid(srcSetBreakpoints: [320, 400, 500, 600, 800, 1024, 1600]) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
    site {
      siteMetadata {
        title
      }
    }
  }
`
