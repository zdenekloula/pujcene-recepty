import React from "react"
import { Link } from "gatsby"
import NavigationLogo from "./NavigationLogo"
import NavigationLogoSmall from "./NavigationLogoSmall"
import ThemeSwitch from "../ThemeSwitch/ThemeSwitch"

const Navigation = props => {
  return (
    <nav className="navigation">
      <div className="grid__container">
        <div className="navigation__wrapper">
          <div className="navigation__logo-wrapper">
            <Link
              className="navigation__logo navigation__logo--large icon"
              to="/"
            >
              <NavigationLogo className="icon__svg" />
            </Link>
            <Link
              className="navigation__logo navigation__logo--small icon"
              to="/"
            >
              <NavigationLogoSmall className="icon__svg" />
            </Link>
          </div>
          <div className="navigation__theme-switch">
            <ThemeSwitch />
          </div>
          <ul className="navigation__list">
            <li className="navigation__item">
              <Link className="navigation__link" to="/kategorie/snidane/">
                Snídaně
              </Link>
            </li>
            <li className="navigation__item">
              <Link className="navigation__link" to="/kategorie/obedy/">
                Obědy
              </Link>
            </li>
            <li className="navigation__item">
              <Link className="navigation__link" to="/kategorie/vecere/">
                Večeře
              </Link>
            </li>
            <li className="navigation__item">
              <Link className="navigation__link" to="/kategorie/jine/">
                Jiné
              </Link>
            </li>
          </ul>
          <div className="navigation__hamburger">
            <button
              className="navigation__hamburger-item navigation__hamburger-item--open"
              onClick={props.setIsOpened}
            >
              <div className="navigation__hamburger-icon icon">
                <svg
                  className="icon__svg"
                  version="1.1"
                  xmlns="http://www.w3.org/2000/svg"
                  x="0px"
                  y="0px"
                  width="22.42px"
                  height="16.02px"
                  viewBox="0 0 22.42 16.02"
                >
                  <rect width="22.42" height="3.2" />
                  <rect y="6.41" width="22.42" height="3.2" />
                  <rect y="12.81" width="22.42" height="3.2" />
                </svg>
              </div>
            </button>
            <button
              className="navigation__hamburger-item navigation__hamburger-item--close"
              onClick={props.setIsOpened}
            >
              <div className="navigation__hamburger-icon icon">
                <svg
                  className="icon__svg"
                  version="1.1"
                  xmlns="http://www.w3.org/2000/svg"
                  x="0px"
                  y="0px"
                  width="20px"
                  height="20px"
                  viewBox="0 0 11 11"
                >
                  <path
                    d="M6.21,5.5l4.65-4.65c0.2-0.2,0.2-0.51,0-0.71s-0.51-0.2-0.71,0L5.5,4.79L0.85,0.15c-0.2-0.2-0.51-0.2-0.71,0
                s-0.2,0.51,0,0.71L4.79,5.5l-4.65,4.65c-0.2,0.2-0.2,0.51,0,0.71C0.24,10.95,0.37,11,0.5,11s0.26-0.05,0.35-0.15L5.5,6.21l4.65,4.65
                c0.1,0.1,0.23,0.15,0.35,0.15s0.26-0.05,0.35-0.15c0.2-0.2,0.2-0.51,0-0.71L6.21,5.5z"
                  />
                </svg>
              </div>
            </button>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Navigation
