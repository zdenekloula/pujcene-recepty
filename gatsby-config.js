module.exports = {
  siteMetadata: {
    title: `Půjčené recepty`,
    description: `Nalezené, přebrané a půjčené recepty. V každém receptu troška inspirace.`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: "gatsby-source-wordpress",
      options: {
        // The base url to your WP site.
        baseUrl: "pujcene-recepty-wp.4fan.cz",
        // WP.com sites set to true, WP.org set to false
        hostingWPCOM: false,
        // The protocol. This can be http or https.
        protocol: "http",
        // Use 'Advanced Custom Fields' Wordpress plugin
        useACF: false,
        auth: {},
        // Set to true to debug endpoints on 'gatsby build'
        verboseOutput: false,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Půjčené Recepty`,
        short_name: `Půjčené Recepty`,
        start_url: `/`,
        background_color: `#120240`,
        theme_color: `#120240`,
        display: "standalone",
        icon: `src/images/logo-square.svg`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    `gatsby-plugin-offline`,
    `gatsby-plugin-sass`,
  ],
}
