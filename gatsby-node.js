const _ = require("lodash")
const path = require("path")
const { createFilePath } = require("gatsby-source-filesystem")
// const { paginate } = require("gatsby-awesome-pagination")

// const getOnlyPublished = edges =>
//   _.filter(edges, ({ node }) => node.status === "publish")

exports.createPages = ({ actions, graphql }) => {
  const { createPage } = actions

  return graphql(`
    {
      allWordpressPost {
        edges {
          node {
            id
            slug
            status
            template
            format
          }
        }
      }
      allWordpressCategory {
        edges {
          node {
            name
            slug
            id
          }
        }
      }
      allWordpressWpAuthors {
        edges {
          node {
            name
            id
            slug
          }
        }
      }
    }
  `).then(result => {
    const allPosts = result.data.allWordpressPost.edges

    const postTemplate = path.resolve(`./src/templates/post.js`)

    allPosts.forEach(edge => {
      createPage({
        path: `/clanek/${edge.node.slug}/`,
        component: postTemplate,
        context: {
          id: edge.node.id,
        },
      })
    })

    const allCategories = result.data.allWordpressCategory.edges

    const categoryTemplate = path.resolve(`./src/templates/category.js`)

    allCategories.forEach(edge => {
      createPage({
        path: `/kategorie/${edge.node.slug}/`,
        component: categoryTemplate,
        context: {
          slug: edge.node.slug,
        },
      })
    })

    const allAuthors = result.data.allWordpressWpAuthors.edges

    const authorTemplate = path.resolve(`./src/templates/author.js`)

    allAuthors.forEach(edge => {
      createPage({
        path: `/autor/${edge.node.slug}/`,
        component: authorTemplate,
        context: {
          slug: edge.node.slug,
        },
      })
    })
  })
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode })
    createNodeField({
      name: `slug`,
      node,
      value,
    })
  }
}
