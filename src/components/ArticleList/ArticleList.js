import React from "react"
import { Link } from "gatsby"
import Image from "gatsby-image"

const ArticleList = ({ firstLarge = false, data }) => {
  return (
    <div className="article-list">
      <div className="article-list__wrapper">
        {data.allWordpressPost.edges.map(({ node }, index) => (
          <div
            className={`article-list__item ${
              firstLarge ? (index === 0 ? "article-list__item--large" : "") : ""
            }`}
            key={node.title}
          >
            {node.featured_media && (
              <Link className="article-list__link" to={`/clanek/${node.slug}`}>
                <div className="article-list__image-wrapper">
                  <Image
                    fluid={node.featured_media.localFile.childImageSharp.fluid}
                    imgStyle={{ objectFit: "cover" }}
                    alt={node.title}
                    className="article-list__image"
                  />
                </div>
              </Link>
            )}
            <div className="article-list__content">
              <Link className="article-list__link" to={`/clanek/${node.slug}`}>
                <h2 className="article-list__title">{node.title}</h2>
              </Link>
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}

export default ArticleList
