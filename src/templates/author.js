import React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"
// import Image from "../components/image"
import SEO from "../components/seo"
import ArticleList from "../components/ArticleList/ArticleList"

const AuthorPage = props => {
  const data = props.data
  return (
    <Layout>
      <SEO title={data.wordpressWpAuthors.name} />
      <div className="content">
        <h1 className="content__title">
          Recepty od {data.wordpressWpAuthors.description}
        </h1>
        <div className="content__wrapper">
          {data.allWordpressPost.edges.length > 0 ? (
            <ArticleList data={data} firstLarge={false} />
          ) : (
            <div className="content__empty">
              <h3>
                Od {data.wordpressWpAuthors.description} nemáme žádné recepty.
              </h3>
            </div>
          )}
        </div>
      </div>
    </Layout>
  )
}
export default AuthorPage

export const pageQuery = graphql`
  query($slug: String!) {
    allWordpressPost(
      filter: { acf: { author: { slug: { eq: $slug } } } }
      sort: { fields: [date], order: [DESC] }
    ) {
      edges {
        node {
          slug
          title
          featured_media {
            localFile {
              childImageSharp {
                fluid(srcSetBreakpoints: [320, 400, 500]) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
    wordpressWpAuthors(slug: { eq: $slug }) {
      name
      path
      id
      description
    }
  }
`
