import React, { useState } from "react"
import PropTypes from "prop-types"
import Navigation from "./Navigation/Navigation"

const Header = ({ siteTitle }) => {
  const [isOpened, setIsOpened] = useState(false)
  const handleOpened = event => {
    event.preventDefault()
    setIsOpened(isOpenedNav => !isOpenedNav)
  }
  return (
    <header className={isOpened ? "is-nav-opened" : ""}>
      <Navigation isOpened={isOpened} setIsOpened={handleOpened} />
    </header>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
