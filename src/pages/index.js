import React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"
// import Image from "../components/image"
import SEO from "../components/seo"
import ArticleList from "../components/ArticleList/ArticleList"

const IndexPage = props => {
  const data = props.data
  return (
    <Layout>
      <SEO title="Domovská stránka" />
      <ArticleList data={data} firstLarge={true} />
    </Layout>
  )
}
export default IndexPage

// Set here the ID of the home page.
export const pageQuery = graphql`
  query {
    allWordpressPost(sort: { fields: [date], order: [DESC] }) {
      edges {
        node {
          title
          excerpt
          slug
          featured_media {
            localFile {
              childImageSharp {
                fluid(srcSetBreakpoints: [320, 400, 500]) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`
