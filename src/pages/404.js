import React from "react"
import { Link } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"

const NotFoundPage = () => (
  <Layout>
    <SEO title="Recept nenalazen" />
    <div className="content">
      <h1 className="content__title">
        Bohužel jsme nenašli vámi požadovaný recept.
        <span role="img" aria-label="sad-emoji">
          😞
        </span>
      </h1>
      <div className="content__wrapper content__wrapper--center">
        <Link to="/">Přejít na domovskou stránku</Link>
      </div>
    </div>
  </Layout>
)

export default NotFoundPage
