import React from "react"
import PropTypes from "prop-types"

export default function HTML(props) {
  return (
    <html {...props.htmlAttributes}>
      <head>
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        {props.headComponents}
      </head>
      <body {...props.bodyAttributes}>
        <script
          dangerouslySetInnerHTML={{
            __html: `
              (function() {
                var preferredTheme;
                function setTheme(newTheme) {
                  preferredTheme = newTheme;
                  window.__theme = preferredTheme;
                  document.body.className = newTheme;
                }
    
                function setPreferredTheme(newTheme) {
                  setTheme(newTheme);
                  try {
                    localStorage.setItem('theme', newTheme);
                  } catch (err) {}
                }
                window.setPreferredTheme = setPreferredTheme
    
                try {
                  preferredTheme = localStorage.getItem('theme');
                } catch (err) {
                  console.log(err)
                }
                
                var darkQuery = window.matchMedia('(prefers-color-scheme: dark)');
                darkQuery.addListener(function(e) {
                  setPreferredTheme(e.matches ? 'dark' : 'light')
                });
    
                setTheme(preferredTheme || (darkQuery.matches ? 'dark' : 'light'));
              })();
              `,
          }}
        />
        {props.preBodyComponents}
        <noscript key="noscript" id="gatsby-noscript">
          Ke správné funkcionalitě webu si prosím zapněte JavaScript.
        </noscript>
        <div
          key={`body`}
          id="___gatsby"
          dangerouslySetInnerHTML={{ __html: props.body }}
        />
        {props.postBodyComponents}
      </body>
    </html>
  )
}

HTML.propTypes = {
  htmlAttributes: PropTypes.object,
  headComponents: PropTypes.array,
  bodyAttributes: PropTypes.object,
  preBodyComponents: PropTypes.array,
  body: PropTypes.string,
  postBodyComponents: PropTypes.array,
}
